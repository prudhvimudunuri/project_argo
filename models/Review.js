var mongoose = require('mongoose')

var Reviews = new mongoose.Schema({
name: {type:String, default:''},
rating: {type: String, default: ''},
feedback: {type:String, default:''}

})


module.exports = mongoose.model('Reviews', Reviews)
