import React, {Component} from 'react'
import Review from '../presentation/Review'
import styles from './styles'
import superagent from 'superagent'

class Reviews extends Component {

    constructor() {
        super()
        this.state = { //you never change state you make a copy and play with that
            review: {
                name: '',
                rating: '',
                recommend: '',
                feedback: ''
            },
            list: []
        }
    }
    componentDidMount() {
        console.log('componentDidMount: ')

        superagent.get('https://api.mongolab.com/api/1/databases/reviews/collections/reviews/?apiKey=LwXMI0A5YY5a1Oww9yCBc7UdlzCFWpA2').query(null).set('Accept', 'application/json').end((err, response) => {

            if (err) {
                alert('ERROR: ' + err)
                return
            }
            console.log(JSON.stringify(response.body))
            let results = response.body
            console.log("rers":results);
            this.setState({list: results})

        })

    }

    submitReview() {
        console.log('submitReview:' + JSON.stringify(this.state.review))
        let updatedList = Object.assign([], this.state.list)
        updatedList.push(this.state.review)
        this.setState({list: updatedList})
        var data = this.state.review
        console.log("Post call", data)
        superagent.post('https://api.mongolab.com/api/1/databases/reviews/collections/reviews/?apiKey=LwXMI0A5YY5a1Oww9yCBc7UdlzCFWpA2').send(data).set('Accept', 'application/json').end((err, response) => {

            if (err) {
                alert('ERROR: ' + err)
                return
            }
            console.log("post success");

        })
    }

    updateName(event) {
        console.log('updateName ' + event.target.value)
        let updatedReview = Object.assign({}, this.state.review)
        updatedReview['name'] = event.target.value
        this.setState({ //you never change state you make a copy and play with that
            review: updatedReview
        })
    }

    updateRating(event) {
        console.log('updateRating' + event.target.value)
        let updatedReview = Object.assign({}, this.state.review)
        updatedReview['rating'] = event.target.value

        this.setState({ //you never change state you make a copy and play with that
            review: updatedReview
        })

    }

    updateFeedback(event) {
        console.log('updateFeedback' + event.target.value)
        let updatedReview = Object.assign({}, this.state.review)
        updatedReview['feedback'] = event.target.value

        this.setState({ //you never change state you make a copy and play with that
            review: updatedReview
        })


    }

    render() {
        const listItems = this.state.list.map((review, i) => {
            return (
                <div key={i}><Review currentReview={review}/></div>
            )
        })

        return (
            <div>
                <div>
                    {listItems}
                </div>
                <input onChange={this.updateName.bind(this)} className="form-control" type='text' placeholder='Your Name'/>
                <br/>
                <input onChange={this.updateRating.bind(this)} className="form-control" type='text' placeholder='Give Rating'/>
                <br/>
                <input onChange={this.updateFeedback.bind(this)} className="form-control" type='text' placeholder='Feedback'/>
                <br/>
                <button onClick={this.submitReview.bind(this)} className="btn btn-info">
                    Submit Review
                </button>
                <br/>
            </div>

        )
    }
}

export default Reviews
