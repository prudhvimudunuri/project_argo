import React, { Component } from 'react'
import Profile from '../presentation/Profile'
import styles from './styles'
import superagent from 'superagent'
var Formio = require('react-formio');


class Profiles extends Component {

	constructor(){
		super()
		this.state = { //you never change state you make a copy and play with that
			profile: {
				name:'',
				body:''
			},

			list: [

			]
		}

	}

	componentDidMount(){
		console.log('componentDidMount: ')

		superagent
		.get('https://api.mongolab.com/api/1/databases/reviews/collections/profiles/?apiKey=LwXMI0A5YY5a1Oww9yCBc7UdlzCFWpA2')
		.query(null)
		.set('Accept','application/json')
		.end((err, response) => {

			if(err){
				alert('ERROR: ' + err)
				return
			}
			console.log(JSON.stringify(response.body))
			let results = response.body
			this.setState({
				list: results
			})

		})

	}

	submitProfile(){
		console.log('submitProfile:' + JSON.stringify(this.state.profile))
		let updatedList = Object.assign([], this.state.list)
		updatedList.push(this.state.profile)

		this.setState({
			list: updatedList
		})
		var data = this.state.profile
		console.log("Post call", data)
		superagent.post('https://api.mongolab.com/api/1/databases/reviews/collections/profiles/?apiKey=LwXMI0A5YY5a1Oww9yCBc7UdlzCFWpA2').send(data).set('Accept', 'application/json').end((err, response) => {

				if (err) {
						alert('ERROR: ' + err)
						return
				}
				console.log("post success");

		})
	}

	updateName(event){
		console.log('updateName ' + event.target.value)

		let updatedProfile = Object.assign({}, this.state.profile)
		updatedProfile['name'] = event.target.value

		this.setState({//you never change state you make a copy and play with that
			profile: updatedProfile
		})
	}

	updateBody(event){
		console.log('updateBody' + event.target.value)
		let updatedProfile = Object.assign({}, this.state.profile)
		updatedProfile['body'] = event.target.value

		this.setState({//you never change state you make a copy and play with that
			profile: updatedProfile
		})

	}


	render(){

		const listItems = this.state.list.map((profile, i) => {
			return (
				<div key={i}><Profile currentProfile={profile} />

				</div>
			)
		})


		return (
			<div>
				<div>
					{listItems}
				</div>
				<input onChange={this.updateName.bind(this)} className="form-control" type='text' placeholder='Name'/> <br />
				<input onChange={this.updateBody.bind(this)} className="form-control" type='text' placeholder='Position'/> <br />
				<button onClick={this.submitProfile.bind(this)} className = "btn btn-info"> Submit Profile </button> <br />

		</div>

		)
	}
}

export default Profiles
