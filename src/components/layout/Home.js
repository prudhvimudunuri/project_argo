import React,  { Component } from 'react'
import Profiles from '../containers/Profiles'
import Reviews from '../containers/Reviews'


class Home extends Component {

	render(){
		return (
			<div className="container">
				<div className = "row">
						<div className = "col-md-6">
							<Profiles />
						</div>
						<div className = "col-md-6">
							<Reviews />
						</div>
				</div>	
			</div>
		)
	}
}

export default Home