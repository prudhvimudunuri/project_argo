import React, {Component} from 'react'
import styles from './styles'

class Review extends Component {

    render() {
        const reviewStyle = styles.review

        return (
            <div>
                <h1>User Responses</h1>
                <div style={reviewStyle.container}>
                    <h2 style={reviewStyle.header}>
                      <div>
                        <h3>Name:</h3>
                        </div>
                        <a style={reviewStyle.title}>{this.props.currentReview.name}</a>
                    </h2>
                    <h3>Rating:</h3>
                    <div style={reviewStyle.title}>{this.props.currentReview.rating}</div>
                    <br/>
                    <h3>Feedback:</h3>
                    <span style={reviewStyle.title}>{this.props.currentReview.feedback}</span>
                </div>
            </div>
        )
    }
}

export default Review
