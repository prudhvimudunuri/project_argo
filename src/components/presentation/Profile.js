import React, { Component } from 'react'
var Formio = require('react-formio');
import styles from './styles'


class Profile extends Component  {

	render(){
		const profileStyle = styles.profile

		return (
			<div>
			<h1> User Profiles</h1>
			<div style= {profileStyle.container}>
				<h2 style={profileStyle.header}> </h2>
					<a style={profileStyle.title}>{this.props.currentProfile.name} </a>
					<p style={profileStyle.p}>{this.props.currentProfile.body} </p>
			</div>
			
		</div>
		)
	}
}



export default Profile
