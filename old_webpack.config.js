var webpack = require('webpack')
var path = require('path')


module.exports = {

	entry: { //react code i write please take this code and put it in output
		app: './src/app.js'
	},
	output:{
		filename: 'public/build/bundle.js',
		sourceMapFilename: 'public/build/bundle.map'

	},
	devtool: '#source-map',
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules)/,
				loader: 'babel',
				query: {
				presets: ['react', 'es2015']
				}

			}
			
		]

	},
}