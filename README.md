#  Argo - Review forum App

This is a project developed as a part of coding battle for Full Stack Developer position at ARGO.
This is a prototype developed to mock the survey form.

#### How to run the project:

 * open new terminal and run `mongod`
 * open new terminal and run `npm install`
 * `webpack`  
 * open browser and find app at `localhost:3000`





The prototype persists data in MongoDB using API written in Node.js

#### Data Persisted:
1. Review details
 * Name
 * Rating
 * Feedback
 
 2. User Profiles
 * Name
 * Job Position

UI developed in ReactJS :+1:

```javascript
Container Components:
	* Profiles
    * Reviews
Presentational Components:
	* Profile
    * Review
```



### Techonology Stack:

 * [react.js](https://facebook.github.io/react/) for UI rendering
 * [SuperAgent](https://github.com/visionmedia/superagent) for making calls to API
 * [Node.js](https://nodejs.org/en/) for developing API
 * [Express](https://expressjs.com/) for listening to network requests

