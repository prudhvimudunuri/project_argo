var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/createprofile', function(req, res, next) {
  res.render('createprofile', null);
});

router.get('/createreview', function(req, res, next) {
  res.render('createreview', null);
});



module.exports = router;
