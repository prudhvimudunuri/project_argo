var ReviewController = require('./ReviewController')
var ProfileController = require('./ProfileController')

module.exports = {
	profile: ProfileController,
	review: ReviewController
}