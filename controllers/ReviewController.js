var Review = require('../models/Review')
// originally zone
module.exports = {

	find: function(params, callback){
		Review.find(params, function(err,reviews){
			if(err){
				callback(err, null) //error always comes first
				return
			}

			callback(null,reviews) // zones is the payload
		})

	},
	//
	findById: function(id, callback){
		Review.findById(id, function(err, review){
			if(err) {
				callback(err, null)
				return
			}

			callback(null,review)
		})

	},
	create: function(params, callback){
		console.log("params",params);

			Review.create(params,function(err,review){
				if(err){
					callback(err, null)
					return
				}

				callback(null, review)

			})
	},

	update: function(id, params, callback){
		Review.findByIdAndUpdate(id, params, {new:true}, function(err, review){
			if(err){
				callback(err,null)
				return
			}
			callback(null, review)
		})

	},

	delete: function(id, callback){
		Review.findByIdAndRemove(id, function(err){
			if(err){
				callback(err, null)
				return
			}
			callback(null, null)
		})
	}

}
